package ru.t1.avfilippov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.IUserOwnedRepository;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService <M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M>  {

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    void removeAll(@Nullable String userId);
    
}
