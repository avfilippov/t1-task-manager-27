package ru.t1.avfilippov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "update project by index";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getProjectService().updateByIndex(userId, index, name, description);
    }

}
